package com.example.binar.service;

import com.example.binar.entity.Karyawan;

import java.util.Map;

public interface KaryawanService {
    public Map insert(Karyawan karyawan);

    public Map update(Karyawan karyawan);

    public Map getAll();

    public Map getById(Long idKaryawan);
}
