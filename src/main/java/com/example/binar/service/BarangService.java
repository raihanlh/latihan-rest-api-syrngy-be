package com.example.binar.service;

import com.example.binar.entity.Barang;

import java.util.Map;

public interface BarangService {
    public Map insert(Barang barang);

    public Map update(Barang barang);

    public Map delete(Long idBarang);

    public Map getAll();
}
