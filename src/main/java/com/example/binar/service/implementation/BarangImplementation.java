package com.example.binar.service.implementation;

import com.example.binar.entity.Barang;
import com.example.binar.helper.ResponseHelper;
import com.example.binar.repository.BarangRepo;
import com.example.binar.service.BarangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class BarangImplementation implements BarangService {
    @Autowired
    BarangRepo repo;

    @Override
    public Map insert(Barang barang) {
        Map<String, Object> map = new HashMap<>();
        ResponseHelper respHelper = new ResponseHelper();
        try {
            Barang obj = repo.save(barang);
            respHelper.success(obj, "Sukses menambah barang");

            return map;
        } catch (Exception e) {
            e.printStackTrace();
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map update(Barang barang) {
        Map<String, Object> map = new HashMap<>();
        ResponseHelper respHelper = new ResponseHelper();
        try {
            Barang obj = repo.getByID(barang.getId());

            if (obj == null) {
                return respHelper.notFound();
            }
            obj.setNama(barang.getNama());
            obj.setHarga(barang.getHarga());
            obj.setSatuan(barang.getSatuan());
            obj.setStok(barang.getStok());

            repo.save(obj);
            respHelper.success(obj, "Sukses memperbarui barang");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map delete(Long idBarang) {
        Map<String, Object> map = new HashMap<>();
        ResponseHelper respHelper = new ResponseHelper();
        try {
            Barang obj = repo.getByID(idBarang);

            if (obj == null) {
                return respHelper.notFound();
            }
            repo.deleteById(idBarang);
            respHelper.success(obj, "Sukses menghapus barang");

            return map;
        } catch (Exception e) {
            e.printStackTrace();
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map getAll(){
        List<Barang> list = new ArrayList<Barang>();
        Map map = new HashMap();
        ResponseHelper respHelper = new ResponseHelper();
        try {
            list = (List<Barang>) repo.findAll();

            return respHelper.success(list, "Sukses menampilkan semua barang");

        } catch (Exception e) {
            e.printStackTrace();
            return respHelper.internalServerError(e);
        }
    }
}
