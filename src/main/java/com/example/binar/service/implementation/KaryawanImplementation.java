package com.example.binar.service.implementation;

import com.example.binar.entity.Karyawan;
import com.example.binar.helper.ResponseHelper;
import com.example.binar.repository.KaryawanRepo;
import com.example.binar.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class KaryawanImplementation implements KaryawanService {
    @Autowired
    KaryawanRepo karyawanRepo;

    @Override
    public Map insert(Karyawan karyawan) {
        Map map = new HashMap<>();
        ResponseHelper respHelper = new ResponseHelper();
        try {
            Karyawan obj = karyawanRepo.save(karyawan);
            return respHelper.success(obj, "Data karyawan sukses dibuat");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map update(Karyawan karyawan) {
        Map map = new HashMap<>();
        ResponseHelper respHelper = new ResponseHelper();
        try {
            Karyawan obj = karyawanRepo.getById(karyawan.getId());

            if (obj == null) {
                return respHelper.notFound();
            }

            obj.setNama(karyawan.getNama());
            obj.setAlamat(karyawan.getAlamat());
            obj.setTanggalLahir(karyawan.getTanggalLahir());
            obj.setStatus(karyawan.getStatus());
            karyawanRepo.save(obj);
            return respHelper.success(obj, "Data karyawan sukses diperbarui");
        } catch (Exception e) {
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map getAll() {
        List<Karyawan> list = new ArrayList<Karyawan>();
        Map map = new HashMap<>();
        ResponseHelper respHelper = new ResponseHelper();
        try {
            list = (List<Karyawan>) karyawanRepo.findAll();

            return respHelper.success(list, "Sukses menampilkan semua data karyawan");

        } catch (Exception e) {
            e.printStackTrace();
            return respHelper.internalServerError(e);
        }
    }

    @Override
    public Map getById(Long idKaryawan) {
        Map map = new HashMap<>();
        ResponseHelper respHelper = new ResponseHelper();
        try {
            Karyawan obj = karyawanRepo.getById(idKaryawan);

            if (obj == null) {
                return respHelper.notFound();
            }

            return respHelper.success(obj, "Sukses menampilkan data karyawan");

        } catch (Exception e) {
            e.printStackTrace();
            return respHelper.internalServerError(e);
        }
    }
}
