package com.example.binar.entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Mahasiswa { // POJO model
    String nama;
    String nim;
}
