package com.example.binar.repository;

import com.example.binar.entity.Barang;
import com.example.binar.entity.Karyawan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface KaryawanRepo extends PagingAndSortingRepository<Karyawan, Long> {
    @Query("SELECT k FROM Karyawan k WHERE k.id = :id")
    public Karyawan getById(@Param("id") long id);

    Page<Barang> findByNamaAndStatus(String nama, Integer status, Pageable pageable);
}
