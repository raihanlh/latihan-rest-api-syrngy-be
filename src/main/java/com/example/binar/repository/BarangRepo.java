package com.example.binar.repository;

import com.example.binar.entity.Barang;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BarangRepo extends PagingAndSortingRepository<Barang, Long> {
    // JPQL
    @Query("SELECT c FROM Barang c WHERE c.id = :id")
    public Barang getByID(@Param("id") long id);

    @Query("SELECT c FROM Barang c")
    public List<Barang> getList();

    // JPA
    Page<Barang> findByNama(String nama, Pageable pageable);

    Page<Barang> findByNamaLike(String nama, Pageable pageable);

    // Native
}
