package com.example.binar.controller;

import com.example.binar.entity.Barang;
import com.example.binar.repository.BarangRepo;
import com.example.binar.service.BarangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "v1/barang")
public class BarangController {
    @Autowired
    BarangService service;

    @Autowired
    BarangRepo repo;

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody Barang objModel) {
        Map map = new HashMap();
        Map obj = service.insert(objModel);

        map.put("Request =", objModel);
        map.put("Response =", obj);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Map> update(@PathVariable("id") Long id, @Valid @RequestBody Barang objModel) {
        Map map = new HashMap();
        objModel.setId(id);
        Map obj = service.update(objModel);

        map.put("Request =", objModel);
        map.put("Response =", obj);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else if ((int) obj.get("status") == 404){
            return new ResponseEntity<Map>(obj, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable("id") Long id) {
        Map map = new HashMap();
        Barang objModel = new Barang();

        Map obj = service.delete(id);

        map.put("Request =", objModel);
        map.put("Response =", obj);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else if ((int) obj.get("status") == 404){
            return new ResponseEntity<Map>(obj, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<Map> getAll() {
        Map map = new HashMap();
        Map obj = service.getAll();

//        map.put("Request =", objModel);
        map.put("Response =", obj);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/listbynama")
    public ResponseEntity<Page<Barang>> listbynama(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam() String nama) {
        Pageable show_data = PageRequest.of(page, size);
        System.out.println(nama);
        Page<Barang> list = repo.findByNama(nama, show_data);
        return new ResponseEntity<Page<Barang>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/listbynamalike")
    public ResponseEntity<Page<Barang>> listbynamalike(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam() String nama) {
        Pageable show_data = PageRequest.of(page, size);
        Page<Barang> list = repo.findByNamaLike("%"+nama+"%", show_data);
        return new ResponseEntity<Page<Barang>>(list, new HttpHeaders(), HttpStatus.OK);
    }
}
