package com.example.binar.controller;

import com.example.binar.entity.Karyawan;
import com.example.binar.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "v1/karyawan")
public class KaryawanController {
    @Autowired
    KaryawanService karyawanService;

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody Karyawan objModel) {
        Map map = new HashMap();
        Map obj = karyawanService.insert(objModel);

        map.put("Request =", objModel);
        map.put("Response =", obj);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Map> update(@PathVariable("id") Long id, @Valid @RequestBody Karyawan objModel) {
        Map map = new HashMap();
        objModel.setId(id);
        Map obj = karyawanService.update(objModel);

        map.put("Request =", objModel);
        map.put("Response =", obj);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else if ((int) obj.get("status") == 404){
            return new ResponseEntity<Map>(obj, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<Map> getAll() {
        Map map = new HashMap();
        Map obj = karyawanService.getAll();

//        map.put("Request =", objModel);
        map.put("Response =", obj);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Map> getAll(@PathVariable("id") Long id) {
        Map map = new HashMap();
        Map obj = karyawanService.getById(id);

        map.put("Response =", obj);

        if ((int) obj.get("status") == 200) {
            return new ResponseEntity<Map>(obj, HttpStatus.OK);
        } else if ((int) obj.get("status") == 404){
            return new ResponseEntity<Map>(obj, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Map>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
