package com.example.binar.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/v1/praktek")
public class Example {
    @Value("${spring.application.name}")
    private String appName;

    private static final Logger logger = LoggerFactory.getLogger(Example.class);

    @RequestMapping("/hello")
    @ResponseBody
    public String hello() {
        return "Hello from Spring Boot";
    }

    @PostMapping("/json")
    @ResponseBody
    public Map<String, String> json() {
        HashMap<String, String> map = new HashMap<>();
        map.put("key", "value");
        map.put("foo", "bar");
        map.put("aa", "bb");
        return map;
    }

    @RequestMapping("/test")
    @ResponseBody
    public String test() {
        logger.info("this is a info message");
        logger.warn("this is a warn message");
        logger.error("this is a error message");
        return appName;
    }
}
