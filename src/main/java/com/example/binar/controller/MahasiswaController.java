package com.example.binar.controller;

import com.example.binar.entity.Mahasiswa;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/v1/mhs")
public class MahasiswaController {
    // Wajib static
    private static Map<String, Mahasiswa> mahasiswaRepo = new HashMap<>();

    static { // Dependency Injection
        Mahasiswa mhs =  new Mahasiswa();
        mhs.setNama("Nama 1");
        mhs.setNim("1");

        mahasiswaRepo.put(mhs.getNim(), mhs);

        Mahasiswa mhs2 =  new Mahasiswa();
        mhs2.setNama("Nama 2");
        mhs2.setNim("2");

        mahasiswaRepo.put(mhs2.getNim(), mhs2);
    }

    @RequestMapping(value = "/")
    public ResponseEntity<Object> getMahasiswa() {
        return new ResponseEntity<>(mahasiswaRepo.values(), HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Object> createMahasiswa(@RequestBody Mahasiswa mhs) {
        mahasiswaRepo.put(mhs.getNim(), mhs);
//        return new ResponseEntity<>("Mahasiswa created succesfully", HttpStatus.OK);
        return new ResponseEntity<>(mahasiswaRepo.values(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateMahasiswa(@PathVariable("id") String nim, @RequestBody Mahasiswa mhs) {
        mahasiswaRepo.remove(nim);
        mhs.setNim(nim);
        mahasiswaRepo.put(nim, mhs);
//        return new ResponseEntity<>("Mahasiswa updated succesfully", HttpStatus.OK);
        return new ResponseEntity<>(mahasiswaRepo.values(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteMahasiswa(@PathVariable("id") String id) {
        mahasiswaRepo.remove(id);
//        return new ResponseEntity<>("Mahasiswa deleted succesfully", HttpStatus.OK);
        return new ResponseEntity<>(mahasiswaRepo.values(), HttpStatus.OK);
    }
}
